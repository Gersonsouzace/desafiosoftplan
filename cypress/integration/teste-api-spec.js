/// <reference types="cypress" />

describe('Testes de API desafio Softplan', () => {
    
    it.only('Cadastrar um livro', () => {
        cy.cadastroProduto();
    });
    it('Validar o retorno de todos os livros cadastrados;', () => {
        cy.retornaTodosOsLivros();
    });

    it('Validar retorno de dados do livro desejado', () => {
        cy.retornaUmLivroPeloId();
    });
});