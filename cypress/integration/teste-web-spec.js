/// <reference types="cypress" />

describe('Teste web Softplan', () => {
    beforeEach(() => {
        cy.homePage();
        cy.cadastrandoItem();
    
    });
    it('Verifica se item foi cadastrado', () => {
        cy.get('span.todo-count')
        .should('contain', 'item left');
    });
    it('Excluir itens da lista', () => {
        cy.excluirItem();
        cy.get('.filters')
            .should('not.exist');
    });

    it('Marcar item como completo', () => {
        cy.marcarItemComoCompleto();
        cy.get('input.toggle')
        .should('be.checked');
    });

    it('Selecionar Active', () => {
        cy.selecionaActive();
        cy.get('[data-reactid=".0.2.1.2"] > a')
        .should('have.class', 'selected');
    });

    it('Selecionar Completed', () => {
      cy.selecionaCompled();
      cy.get('[data-reactid=".0.2.1.4"] > a')
      .should('have.class', 'selected');
    });

    it('Estando com o filtro ‘All’ ativo clicar em ‘Clearcompleted', () => {
        cy.marcarItemComoCompleto();
        cy.selecionaCompled();
        cy.limpaOCompled();
        cy.get('.filters')
            .should('not.exist');
        
    });
});
