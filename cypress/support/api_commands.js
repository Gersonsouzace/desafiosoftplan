Cypress.Commands.add("cadastroProduto", () =>{
    cy.request({
        method: 'POST',
        url: 'https://fakerestapi.azurewebsites.net/api/v1/Books',
        body:{
            "id": 900,
            "title": "Livro Teste",
            "description": "Livro teste para desafio",
            "pageCount": 200,
            "excerpt": "testing book",
            "publishDate": "2021-03-18"
            }
    }).then(res=> console.log(res)).as('response');

    cy.get('@response').then(res =>{
       
        expect(res.status).to.be.equal(200);
        expect(res.body.id).to.be.equal(900);
    });
});

Cypress.Commands.add("retornaTodosOsLivros", () =>{
    cy.request({
        method: 'GET',
        url: 'https://fakerestapi.azurewebsites.net/api/v1/Books',
    }).then(res=> console.log(res)).as('response');

    cy.get('@response').then(res =>{
       
        expect(res.status).to.be.equal(200);
        expect(res.body).not.empty;
    })
});

Cypress.Commands.add("retornaUmLivroPeloId", () =>{
    cy.request({
        method: 'GET',
        url: 'https://fakerestapi.azurewebsites.net/api/v1/Books/5',
    }).then(res=> console.log(res)).as('response');

    cy.get('@response').then(res =>{
       
        expect(res.status).to.be.equal(200);
        expect(res.body.id).to.be.equal(5);
    })
});
