// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })



Cypress.Commands.add("homePage", () =>{
    cy.visit("https://todomvc.com/examples/typescript-react/#/completed");
});

Cypress.Commands.add("cadastrandoItem", () =>{
    cy.get('input.new-todo')
    .click()
    .type('teste')
    .type('{enter}');
});


Cypress.Commands.add("excluirItem", () =>{
    cy.get('[data-reactid=".0.2.1.0"] > a')
    .click();
    cy.get('.destroy')
      .invoke('show')
      .click();

});

Cypress.Commands.add("marcarItemComoCompleto", () =>{
    cy.get('[data-reactid=".0.2.1.0"] > a')
    .click();
    cy.get('input.toggle')
      .check()
});

Cypress.Commands.add("selecionaActive", () =>{
    cy.get('[data-reactid=".0.2.1.2"] > a')
    .click()
});

Cypress.Commands.add("selecionaCompled", () =>{
    cy.get('[data-reactid=".0.2.1.4"] > a')
    .click()
});

Cypress.Commands.add("limpaOCompled", () =>{
    cy.get('.clear-completed').click()

    cy.get('.filters')
    .should('not.exist');

});